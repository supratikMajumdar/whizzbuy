//
//  ResetPasswordVerifyMobileViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/23/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVerifyMobileViewController: UIViewController {
    
    
/*** IBOutlets ***/
@IBOutlet weak var messageLabel: UILabel!
@IBOutlet weak var mobileNumberInput: UITextField!
@IBOutlet weak var activityIndicatorViewHolder: UIView!
@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
/*** Button Outlets ***/
@IBOutlet weak var proceedButton: UIButton!
    
/*** NSConstraints Outlets ***/
@IBOutlet weak var proceedButtonBottomConstraint: NSLayoutConstraint!
    
/*** Alamofire Manger ***/
var forgotPasswordOTPgenAlampfireManager : Alamofire.Manager?
    
/*** Keyboard is Visible ***/
    var keyBoardVisible: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*** Make Mobile Number Input First Responder ***/
        mobileNumberInput.becomeFirstResponder()
        
        /*** Alamofire Timeout Setup ***/
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 10 // seconds
        configuration.timeoutIntervalForResource = 10
        forgotPasswordOTPgenAlampfireManager = Alamofire.Manager(configuration: configuration)

        /*** Keyboard Event Listeners ***/
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ForgotPasswordVerifyMobileViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ForgotPasswordVerifyMobileViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        
        /*** Initial Setup For Activity Indicator ***/
        activityIndicatorViewHolder.layer.cornerRadius = 3.0
        activityIndicatorViewHolder.hidden = true
        
        /*** Initial Setup For mobileInput ***/
        mobileNumberInput.layer.cornerRadius = 5.0
        mobileNumberInput.layer.borderColor = UIColor(red: 230, green: 230, blue: 232).CGColor
        mobileNumberInput.layer.borderWidth = 1.5
        /*** Input Paddings ***/
        let paddingViewMobileNumberInput = UIView(frame: CGRectMake(0, 0, 8, self.mobileNumberInput.frame.height))
        mobileNumberInput.leftView = paddingViewMobileNumberInput
        mobileNumberInput.leftViewMode = UITextFieldViewMode.Always

        /*** Initial Setup For proceedButton ****/
        proceedButton.layer.cornerRadius = 5.0
        proceedButton.userInteractionEnabled = false
        proceedButton.alpha = 0.5
        
        
        mobileNumberInput.addTarget(self, action: #selector(ForgotPasswordVerifyMobileViewController.mobileNumberInputDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*** Function For Mobile Input Editing Changed ***/
    func mobileNumberInputDidChange (sender: UITextField){
        
        if mobileNumberInput.text?.characters.count == 10 {
            proceedButton.userInteractionEnabled = true
            proceedButton.alpha = 1.0
        }
        else {
            proceedButton.userInteractionEnabled = false
            proceedButton.alpha = 0.5
        }
        
    }
    
    /*** IBAction for Proceed Button ***/
   
    @IBAction func proceedButtonPressed(sender: UIButton) {
        let mobileNumber: String = (mobileNumberInput.text)!
        resignFirstResponderInView()
        startActivityIndicator()
        forgotPasswordOTPgen(mobileNumber)
        
    }
    
    
    /*** IBAction for Close Button ****/
    @IBAction func closeButtonPressed(sender: AnyObject) {
        resignFirstResponderInView()
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    /*** Keyboard Handling Functions Begins***/
    
    func resignFirstResponderInView(){
        
        UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, forEvent: nil)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    func keyboardWillShow(notification: NSNotification){
        
        
        if keyBoardVisible == false{
            keyBoardVisible = true
            adjustHeight(notification)
        }
    }
    
    
    func keyboardWillHide (notification: NSNotification){
        keyBoardVisible = false
        adjustHeight(notification)
    }

    func adjustHeight(notification: NSNotification){
        var userInfo = notification.userInfo!
        let keyboardFrame: CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let animationDuration = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSTimeInterval + 5000
        
        let changeInProccedButtonBottonConstraint  = (CGRectGetHeight(keyboardFrame) + 7)
        
        UIView.animateWithDuration(animationDuration) { 
            
            if (self.keyBoardVisible == true){
                self.proceedButtonBottomConstraint.constant = changeInProccedButtonBottonConstraint * 1
            }
            else
                if(self.keyBoardVisible == false){
                    self.proceedButtonBottomConstraint.constant = 8
            }
            
        }
    }
    
    
    /*** API Call to Forgot Password OTP Gen Begins ***/
    func forgotPasswordOTPgen(mobileNumber: String){
        
        let forgotPasswordOTPgenURL = "https://whizzbuydev.herokuapp.com/forgotpasswordOTPgen"
        
        let forgotPasswordOTPgenParameters = [
            "MobileNumber": mobileNumber
        ]
        print(forgotPasswordOTPgenParameters)
        
        self.forgotPasswordOTPgenAlampfireManager?.request(.POST, forgotPasswordOTPgenURL, parameters: forgotPasswordOTPgenParameters, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) in
            self.stopActivityIndicator()
            if let JSON = response.result.value {
                print(JSON)
                if let statusCode =  JSON["Status"] as! String?{
                    self.forgotPasswordOTPgenApiHandling(Int(statusCode)!)
                }
            }
            else{
                let networkErrorAlert = UIAlertController(title: "Reset Password", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)
                
                networkErrorAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                
                self.presentViewController(networkErrorAlert, animated: true, completion: nil)
                
                print("Network Error")
            }

            
        })
        
    }
    /*** API Call to Forgot Password OTP Gen Ends ***/
    
    /*** API Handling for Forgot Password OTP Gen Begins ***/
    func forgotPasswordOTPgenApiHandling(statusCode: Int){
        
        if statusCode == 200 {
            //TODO: Segue to Verify OTP
            performSegueWithIdentifier("ForgotPasswordVerifyOtpSegue", sender: nil)
        }
        else
            if statusCode == 1000{
            
                let mobileNumberNotRegisteredAlert = UIAlertController(title: "Reset Password", message: "Mobile Number is not registered with Whizzbuy", preferredStyle: UIAlertControllerStyle.Alert)
               
            mobileNumberNotRegisteredAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                
                dispatch_async(dispatch_get_main_queue(), { 
                    
                    self.mobileNumberInput.text = ""
                    
                })
                
                
                }))
                
                self.presentViewController(mobileNumberNotRegisteredAlert, animated: true, completion: nil)
                
            }
                
        else{
            let defaultAlert = UIAlertController(title: "Reset Whizzbuy", message: "Try Again", preferredStyle: UIAlertControllerStyle.Alert)
                
            defaultAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                
            self.presentViewController(defaultAlert, animated: true, completion: nil)
        }
    }
    
    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/

    
    /*** Functions For Hanldling Keyboard Ends ****/
    
    
    // MARK: - Navigation

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        let forgotPasswordOtpVC: ForgotPasswordVerifyOtpViewController = segue.destinationViewController as! ForgotPasswordVerifyOtpViewController
        
        /*** Pass Mobile Number to forgot password Verify OTP View Controller ***/
        forgotPasswordOtpVC.enteredMobileNumber = (mobileNumberInput.text)!
    
    }
    

}
