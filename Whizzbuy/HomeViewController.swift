//
//  HomeViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/6/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UIScrollViewDelegate {

    /*** Premium Ads Outlets ***/
    @IBOutlet weak var premiumAdsScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    /*** Constraint Outlets ***/
    @IBOutlet weak var premiumAdsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var savedAdsButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var spendingTrendsButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var spacingConstraintLifestyleLeftElements: NSLayoutConstraint!
    @IBOutlet weak var spacingConstaintRightLifestyleElements: NSLayoutConstraint!
    
    /*** UI Outlets ***/
    @IBOutlet weak var lifestylesViewHolder: UIView!
    @IBOutlet weak var promotionsImageHolder: UIImageView!
    @IBOutlet weak var savedAdsButton: UIButton!
    @IBOutlet weak var spendingTrendsButton: UIButton!
    //@IBOutlet weak var shoppingBagViewHolder: UIView!
    
    /*** Lifestly View Holder UI Elements ***/
    @IBOutlet weak var lifestyleImageOne: UIImageView!
    @IBOutlet weak var lifestyleImageTwo: UIImageView!
    @IBOutlet weak var lifestyleImageThree: UIImageView!
    @IBOutlet weak var addLifestyleButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lifestylesViewHolder.layer.cornerRadius = 4.0
        
        /*** Saved Ads & Spending Trends Button UI Setup ***/
        savedAdsButton.layer.cornerRadius = 2.0
        savedAdsButton.layer.borderWidth = 2.0
        savedAdsButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        spendingTrendsButton.layer.cornerRadius = 2.0
        spendingTrendsButton.layer.borderWidth = 2.0
        spendingTrendsButton.layer.borderColor = UIColor(red: 12, green: 198, blue: 161).CGColor
        
        /*** Promotions & Deals UI Setup ***/
        promotionsImageHolder.layer.cornerRadius = 2.0
        promotionsImageHolder.layer.borderWidth = 2.0
        promotionsImageHolder.layer.borderColor = UIColor.whiteColor().CGColor
        
        /*** Lifestyle Elements UI Setup ***/
        lifestyleImageOne.layer.cornerRadius = self.lifestyleImageOne.frame.size.width / 2
        lifestyleImageOne.clipsToBounds = true
        lifestyleImageOne.layer.borderWidth = 1.0
        lifestyleImageOne.layer.borderColor = UIColor.grayColor().CGColor
        
        lifestyleImageTwo.layer.cornerRadius = self.lifestyleImageTwo.frame.size.width / 2
        lifestyleImageTwo.clipsToBounds = true
        lifestyleImageTwo.layer.borderWidth = 1.0
        lifestyleImageTwo.layer.borderColor = UIColor.grayColor().CGColor
        
        lifestyleImageThree.layer.cornerRadius = self.lifestyleImageThree.frame.size.width / 2
        lifestyleImageThree.clipsToBounds = true
        lifestyleImageThree.layer.borderWidth = 1.0
        lifestyleImageThree.layer.borderColor = UIColor.grayColor().CGColor
    
        addLifestyleButton.layer.cornerRadius = self.addLifestyleButton.frame.size.width / 2
        addLifestyleButton.clipsToBounds = true
        /*** Lifestyle Elements UI Setup Ends ***/

        
    /*** Actomatically Change Premium Ads ***/
    NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(HomeViewController.moveToNextPage), userInfo: nil, repeats: true)
       
    }

    override func viewWillAppear(animated: Bool) {
        
        setupPremiumAdsView()
        //setSpacingBetweenLifestyleElements()
        setWidthToCenterButtons()
    }
    
    func setWidthToCenterButtons(){
        
        if screenWidth == 320{
            savedAdsButtonWidthConstraint.constant = 150
            spendingTrendsButtonWidthConstraint.constant = 150
            spacingConstraintLifestyleLeftElements.constant = 21
            spacingConstaintRightLifestyleElements.constant = 21
        }
        else
            if screenWidth == 375{
            savedAdsButtonWidthConstraint.constant = 180
            spendingTrendsButtonWidthConstraint.constant = 180
            spacingConstraintLifestyleLeftElements.constant = 36
            spacingConstaintRightLifestyleElements.constant = 36
        }
        else{
                savedAdsButtonWidthConstraint.constant = 210
                spendingTrendsButtonWidthConstraint.constant = 210
                spacingConstraintLifestyleLeftElements.constant = 51
                spacingConstaintRightLifestyleElements.constant = 51
        }
        
    }

    func setupPremiumAdsView(){
        premiumAdsHeightConstraint.constant = getHeightForPremiumAds()
        
        self.premiumAdsScrollView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, premiumAdsHeightConstraint.constant)
        let scrollViewWidth:CGFloat = self.premiumAdsScrollView.frame.width
        let scrollViewHeight:CGFloat = self.premiumAdsScrollView.frame.height
        
        let imgOne = UIImageView(frame: CGRectMake(0, 0,scrollViewWidth, scrollViewHeight))
        imgOne.image = UIImage(named: "Slide 1")
        let imgTwo = UIImageView(frame: CGRectMake(scrollViewWidth, 0,scrollViewWidth, scrollViewHeight))
        imgTwo.image = UIImage(named: "Slide 2")
        let imgThree = UIImageView(frame: CGRectMake(scrollViewWidth*2, 0,scrollViewWidth, scrollViewHeight))
        imgThree.image = UIImage(named: "Slide 3")
        let imgFour = UIImageView(frame: CGRectMake(scrollViewWidth*3, 0,scrollViewWidth, scrollViewHeight))
        imgFour.image = UIImage(named: "Slide 4")
        let imgFive = UIImageView(frame: CGRectMake(scrollViewWidth*4, 0,scrollViewWidth, scrollViewHeight))
        imgFive.image = UIImage(named: "Slide 5")
        let imgSix = UIImageView(frame: CGRectMake(scrollViewWidth*5, 0,scrollViewWidth, scrollViewHeight))
        imgSix.image = UIImage(named: "Slide 6")
        
        self.premiumAdsScrollView.addSubview(imgOne)
        self.premiumAdsScrollView.addSubview(imgTwo)
        self.premiumAdsScrollView.addSubview(imgThree)
        self.premiumAdsScrollView.addSubview(imgFour)
        self.premiumAdsScrollView.addSubview(imgFive)
        self.premiumAdsScrollView.addSubview(imgSix)
        
        self.premiumAdsScrollView.contentSize = CGSizeMake(self.premiumAdsScrollView.frame.width * 6, scrollViewHeight)
        self.premiumAdsScrollView.delegate = self
        self.pageControl.currentPage = 0
    }
    
    func getHeightForPremiumAds() -> CGFloat{
        
        if screenHeight == 568.0{
            
            return 220
        }
        
        else
            if screenHeight == 667 {
                return 290
        }
        
            
        else{
            return 320
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView){
        
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
    }


    func moveToNextPage (){
        
        // Move to next page
        let pageWidth:CGFloat = CGRectGetWidth(self.premiumAdsScrollView.frame)
        let maxWidth:CGFloat = pageWidth * 6
        let contentOffset:CGFloat = self.premiumAdsScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
            
        }
        self.premiumAdsScrollView.scrollRectToVisible(CGRectMake(slideToX, 0, pageWidth, CGRectGetHeight(self.premiumAdsScrollView.frame)), animated: true)
    }
    

}
