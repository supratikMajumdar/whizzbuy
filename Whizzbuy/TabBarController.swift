//
//  TabBarController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/16/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupMiddleButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupMiddleButton() {
        
        var menuButton = UIButton(type: UIButtonType.Custom)
        menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 63, height: 63))
        
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - 3
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        
        menuButton.backgroundColor = UIColor.init(red: 12, green: 198, blue: 161)
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        self.view.addSubview(menuButton)
        
        menuButton.setImage(UIImage(named: "shopping-bag"), forState: UIControlState.Normal)
        
        
        /*** Function For Shopping Button ***/
        menuButton.addTarget(self, action: #selector(TabBarController.menuButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.layoutIfNeeded()
    }
    
    
    func menuButtonAction(sender: UIButton){
        
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        self.performSegueWithIdentifier("segueToShoppingCartView", sender: sender)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

   
    
}
