//
//  ViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 2/27/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import Alamofire

/*** Screen Dimensions ***/
var screenHeight: CGFloat = 0
var screenWidth: CGFloat = 0

class SignInViewController: UIViewController {
   
/*** UI Elements Constraints Outlets ***/
@IBOutlet weak var registerBottomConstraint: NSLayoutConstraint!
@IBOutlet weak var logInToWhizzbuyTopConstraint: NSLayoutConstraint!
@IBOutlet weak var inputFieldsHolderTopConstraint: NSLayoutConstraint!
    
/*** UI Elements Outlets ***/
@IBOutlet weak var inputFieldsHolderView: UIView!
@IBOutlet weak var mobileNumberInput: UITextField!
@IBOutlet weak var passwordInput: UITextField!
@IBOutlet weak var showPasswordButton: UIButton!
@IBOutlet weak var logInButton: UIButton!
@IBOutlet weak var activityIndicatorViewHolder: UIView!
@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
/*** Input Field Counters ***/
var mobileNumberInputLength = 0
var passwordInputLength = 0
    
/*** KeyBoard Is Visible ***/
var keyBoardVisible: Bool = false
var productsInCart = ""
    
/*** AlamoFireManager ***/
var logInAlamoFireManager : Alamofire.Manager?
    
override func viewDidLoad() {
    NSUserDefaults.standardUserDefaults().setObject(productsInCart, forKey: "shoppingCart")
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    /*** Alamofire Timeout Setup ***/
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.timeoutIntervalForRequest = 10 // seconds
    configuration.timeoutIntervalForResource = 10
    logInAlamoFireManager = Alamofire.Manager(configuration: configuration)

    
    
    /*** Get Screen Height ***/
    screenHeight = UIScreen.mainScreen().bounds.height
    screenWidth = UIScreen.mainScreen().bounds.width
    print(screenWidth)
    print(screenHeight)
    
    /*** Activity Indicator Setup ***/
    self.view.bringSubviewToFront(activityIndicatorViewHolder)
    activityIndicatorViewHolder.layer.cornerRadius = 3.0
    activityIndicatorViewHolder.hidden = true
    
    /*** Keyboard Event Listeners ***/
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignInViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignInViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    
    
    /*** Input Fields Setup ***/
        inputFieldsHolderView.layer.cornerRadius = 5.0
        inputFieldsHolderView.layer.borderColor = UIColor(red: 230, green: 230, blue: 232).CGColor
        inputFieldsHolderView.layer.borderWidth = 1.5
        mobileNumberInput.layer.addBorder(UIRectEdge.Bottom, color: UIColor(red: 230, green: 230, blue: 232), thickness: 1.0)
        passwordInput.secureTextEntry = true
        logInButton.userInteractionEnabled = false
        logInButton.layer.cornerRadius = 3.0
        showPasswordButton.hidden = true
    
    
    
    /*** Input Paddings ***/
    let paddingViewMobileNumberInput = UIView(frame: CGRectMake(0, 0, 8, self.mobileNumberInput.frame.height))
    mobileNumberInput.leftView = paddingViewMobileNumberInput
    mobileNumberInput.leftViewMode = UITextFieldViewMode.Always
    
    let paddingViewPasswordInput = UIView(frame: CGRectMake(0, 0, 8, self.passwordInput.frame.height))
    passwordInput.leftView = paddingViewPasswordInput
    passwordInput.leftViewMode = UITextFieldViewMode.Always

    /*** Editing Did Begin Event Listensers To Input Fields ***/
    mobileNumberInput.addTarget(self, action: #selector(SignInViewController.mobileNumberInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
    passwordInput.addTarget(self, action: #selector(SignInViewController.passwordInputDidChange), forControlEvents: UIControlEvents.EditingChanged)

    
    }
    /*** View Did Load Ends ***/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        /*** Clear Text Fields ***/
        mobileNumberInput.text = ""
        passwordInput.text = ""
    }

    /*** IBActions Functions Begins****/
    @IBAction func showPasswordButtonPressed(sender: AnyObject) {
        
        
        self.passwordInput.resignFirstResponder()
        if (passwordInput.secureTextEntry == true){
            //self.resignFirstResponder()
            showPasswordButton.setTitle("HIDE", forState: UIControlState.Normal)
            passwordInput.secureTextEntry = false
         
        }
            
        else
            if (passwordInput.secureTextEntry == false){
                
                showPasswordButton.setTitle("SHOW", forState: UIControlState.Normal)
                passwordInput.secureTextEntry = true
        }
        self.passwordInput.becomeFirstResponder()
        
    }
    
    
    @IBAction func loginButtonPressed(sender: AnyObject) {
       
        let mobileNumber = (mobileNumberInput.text)!
        let password = (passwordInput.text)!
        resignFirstResponderSignInView()
        startActivityIndicator()
        logIn(mobileNumber, password: password)
    
    }
    
    
    @IBAction func registerButtonPressed(sender: UIButton) {
        performRegisterSegue()
    }
    
    /*** IBAction Functions Ends ****/
     
    /*** Perform Register Segue Begins ****/
     
    func performRegisterSegue(){
    
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        
    UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, forEvent: nil)
        
        self.performSegueWithIdentifier("registerSegue", sender: nil)
        
        }

    }
     
    /*** Perform Register Segue Ends ***/
    
    /*** UITextFeild Related Functions Begin ***/
    func mobileNumberInputDidChange(){
        
        mobileNumberInputLength = (mobileNumberInput.text)!.characters.count
        
        if mobileNumberInputLength > 0 && passwordInputLength > 0 {
            //print("mobileNumberInputLength")
            logInButton.userInteractionEnabled = true
        }
        else if mobileNumberInputLength == 0 {
            logInButton.userInteractionEnabled = false
        }
        
    }
    
    func passwordInputDidChange(){
        
        passwordInputLength = (passwordInput.text)!.characters.count
        
        if passwordInputLength > 0{
            showPasswordButton.hidden = false
        }
        
        
        if passwordInputLength > 0 && mobileNumberInputLength > 0{
            //print("mobileNumberInputLength")
            logInButton.userInteractionEnabled = true
        }
            
        else if passwordInputLength == 0 {
            logInButton.userInteractionEnabled = false
            showPasswordButton.hidden = true
        }
        
    }
    /*** UITextFeild Related Functions Ends ***/
    
    /*** Keyboard Handling Functions Begins***/
    
    func resignFirstResponderSignInView(){
    
        UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, forEvent: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
        
        
        if keyBoardVisible == false{
            keyBoardVisible = true
            adjustHeight(notification)
        }
    }
    
    
    func keyboardWillHide (notification: NSNotification){
        keyBoardVisible = false
        adjustHeight(notification)
        
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func adjustHeight(notification: NSNotification){
        
        
    var userInfo = notification.userInfo!
    let keyboardFrame: CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
    let animationDuration = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSTimeInterval + 5000
    let changeInforgotPasswordBottomConstraint = (CGRectGetHeight(keyboardFrame) + 5)
    let changeInInputFieldsHolderTopConstraint = (CGRectGetHeight(keyboardFrame))
        
    UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)

        
        UIView.animateWithDuration(animationDuration) { () -> Void in
            
            if(self.keyBoardVisible == true){
                self.registerBottomConstraint.constant = changeInforgotPasswordBottomConstraint * 1
                self.inputFieldsHolderTopConstraint.constant = changeInInputFieldsHolderTopConstraint * 1 - self.getPosition(screenHeight)
                //self.inputFieldsHolderTopConstraint.constant = changeInInputFieldsHolderTopConstraint * 1 + 50
                
                
                if(screenHeight == 480.0){
                        self.logInToWhizzbuyTopConstraint.constant = -100

                }
                else {
                    
                    self.logInToWhizzbuyTopConstraint.constant = 20

                }
                
            }
                
            else if(self.keyBoardVisible == false){
                self.inputFieldsHolderTopConstraint.constant = UIScreen.mainScreen().bounds.height / 2 - self.inputFieldsHolderView.layer.bounds.height
                self.registerBottomConstraint.constant = 35
                self.logInToWhizzbuyTopConstraint.constant = 121
                
            }
            
        }
    }
    
    
    func getPosition(screenHeight: CGFloat) -> CGFloat{
        
        if(screenHeight == 480.0){
            return 190
        }
        
    else
        if(screenHeight == 568.0){
            return 100
        }
        
        else
            if (screenHeight == 736.0){
            return -50
        }
        else {
                
            return 1
        }
    }
    /*** Keyboard Handling Functions Ends***/
    
     
    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/
    
    /*** Log In API Call Begins ***/
    func logIn(mobileNumber: String, password: String){
        
        let logInApiURL = "https://whizzbuydev.herokuapp.com/login"
        
        let logInParameters = [
            "MobileNumber": mobileNumber,
            "Password": password
        ]
        
        self.logInAlamoFireManager!.request(.POST, logInApiURL, parameters: logInParameters, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            /*** Stop Activity Indicator ***/
            self.stopActivityIndicator()
            
            if let JSON = response.result.value{
                
                if let statusCode = JSON["Status"] as! String?{
                    self.logInApiHandling(Int(statusCode)!)
                }
            }
            else{
                let networkErrorAlert = UIAlertController(title: "Log Into Whizzbuy", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)
                
                networkErrorAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                
                self.presentViewController(networkErrorAlert, animated: true, completion: nil)
                
            print("Network Error")
            }
            
        }
        
    }
    /*** Log In API Call Ends ***/
    
    /*** Log In API Handling Begins ***/
    func logInApiHandling(statusCode: Int){
        if statusCode == 200 {
            
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.performSegueWithIdentifier("segueToHomeScreen", sender: nil)
          })
            
        }
        else{
            generateAlertLogIn(statusCode)
        }
    }
    
    
    /*** Function To Generate Alerts for Sign In ***/
    func generateAlertLogIn(statusCode: Int){
        
    let alertTitle = "Log In With Wihzzbuy"
    var alertMessage = "Try Again"
    var flag = 0
        
        switch statusCode{
            
        case 1000:
            flag = 1
            alertMessage = "Password is Incorrect. Please Try Again"
            break
        
        case 1001:
            flag = 0
            alertMessage = "Account is already logged in from another Device"
            break
            
        case 1002:
            flag = 2
            alertMessage = "Account has not been verified"
            break
        
        case 1003:
            flag = 3
            alertMessage = "Number not Registered. Join Us!"
            break
        
        default:
            flag = 0
            
        }
        
    let logInAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
     
        if flag == 3{
            logInAlert.addAction(UIAlertAction(title: "Register Now", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) -> Void in
                
                self.performRegisterSegue()
            }))
            
        }
        else if flag == 2{
            logInAlert.addAction(UIAlertAction(title: "Verify Now", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) -> Void in
                
                //TODO: Segue To OTP View
            }))
        }
        
        
    logInAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
        
        self.resetTextFieldsLogIn(flag)
    }))
        
    self.presentViewController(logInAlert, animated: true, completion: nil)
        
    }
    
    
    /*** Function to Reset UITextFieldsSignIn ****/
    func resetTextFieldsLogIn(flag: Int){
        if flag == 0 || flag == 2 || flag == 3{
            mobileNumberInput.text = ""
            passwordInput.text = ""
        }
        else
            if flag == 1 {
            passwordInput.text = ""
        }
        
    }
}

