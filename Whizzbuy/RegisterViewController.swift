//
//  RegisterViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/17/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
//    /*** Generated OTP Value ***/
//    var generatedOTP: String?
    
    /*** Entered Data of Birth Value ***/
     var apiDate = ""
    
    /*** Gender Value Entered ***/
    var genderValue = ""
    
    /*** Platform Value -> API Parameter ***/
     let Platform = "2"
    
    /*** AlmofireManager ***/
    var registerAlamoFireManager : Alamofire.Manager?
    
    /*** Register Button Outlet ***/
    @IBOutlet weak var registerButton: UIButton!
    
    /*** Input Fields Outlets ***/
    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var lastNameInput: UITextField!
    @IBOutlet weak var birthdayInput: UITextField!
    @IBOutlet weak var eMailInput: UITextField!
    @IBOutlet weak var mobileNumberInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var confirmPasswordInput: UITextField!
    @IBOutlet weak var genderInput: UITextField!
    @IBOutlet weak var genderInputSegmentedControl: UISegmentedControl!
    
    /*** Activity Indicator Outlets ***/
    @IBOutlet weak var activityIndicatorViewHolder: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    /*** Scroll View Outlets ***/
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    
    /*** Validation Flag Bits ***/
    var flagFirstName = 0
    var flagLastName = 0
    var flagBirthday = 0
    var flagEmail = 0
    var flagMobileNumber = 0
    var passwordsMatchFlag = 0
    var flagGender = 0
    
    /*** Alert Flags For Inputs ***/
    var alertFlagFirstName = 0
    var alertFlagLastName = 0
    var alertFlagBirthday = 0
    var alertFlagEmail = 0
    var alertFlagMobileNumber = 0
    var alertFlagPassword = 0
    var alertFlagConfirmPassword = 0
    
    /*** Validation Icons Outlets ***/
    @IBOutlet weak var validatorIconFirstName: UIButton!
    @IBOutlet weak var validatorIconLastName: UIButton!
    @IBOutlet weak var validatorIconBirthday: UIButton!
    @IBOutlet weak var validatorIconEmail: UIButton!
    @IBOutlet weak var validatorIconMobileNumber: UIButton!
    @IBOutlet weak var validationIconPassword: UIButton!
    @IBOutlet weak var validationIconConfirmPassword: UIButton!
    
    /*** Top Alert Bar Properties ***/
    var alertVisible: Bool = false
    var alertLabel: UILabel = UILabel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        inputFieldsSetup()
        createAlertLabel()
        
        /*** Alamofire Timeout Setup ***/
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 10 // seconds
        configuration.timeoutIntervalForResource = 10
        registerAlamoFireManager = Alamofire.Manager(configuration: configuration)
        
        /*** For Keyboard Listeners ***/
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RegisterViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RegisterViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil)
        
        
        /*** Editing Changed Event Listensers To Input Fields  Begins ***/
        
        firstNameInput.addTarget(self, action: #selector(RegisterViewController.firstNameInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
        lastNameInput.addTarget(self, action: #selector(RegisterViewController.lastNameInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
        eMailInput.addTarget(self, action: #selector(RegisterViewController.eMailInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
        mobileNumberInput.addTarget(self, action: #selector(RegisterViewController.mobileNumberInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
        passwordInput.addTarget(self, action: #selector(RegisterViewController.passwordInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
        confirmPasswordInput.addTarget(self, action: #selector(RegisterViewController.confirmPasswordDidChange), forControlEvents: UIControlEvents.EditingChanged)
        
         /*** Editing Changed Event Listensers To Input Fields  Ends ***/
    
         
        /*** Date Picker ***/
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: #selector(RegisterViewController.birthdayInputValueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        birthdayInput.inputView = datePicker
        
        /*** Set UI Tool Bar For Date Picker ***/
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        toolBar.barStyle = UIBarStyle.Default
        toolBar.tintColor = UIColor.blueColor()
        toolBar.backgroundColor = UIColor.grayColor()
        
        let doneBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(RegisterViewController.donePressed(_:)))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        toolBar.setItems([flexSpace, doneBarBtn], animated: true)
        birthdayInput.inputAccessoryView = toolBar
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        /*** Editing Did Begin Event Listeners To Input Fields Begins ***/
        
        firstNameInput.addTarget(self, action: #selector(RegisterViewController.firstNameInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        lastNameInput.addTarget(self, action: #selector(RegisterViewController.lastNameInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        birthdayInput.addTarget(self, action: #selector(RegisterViewController.birthdayInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        eMailInput.addTarget(self, action: #selector(RegisterViewController.emailInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        mobileNumberInput.addTarget(self, action: #selector(RegisterViewController.mobileNumberInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        passwordInput.addTarget(self, action: #selector(RegisterViewController.passwordInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        confirmPasswordInput.addTarget(self, action: #selector(RegisterViewController.confirmPasswordInputEditingDidBegin(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        
        /*** Editing Did Begin Event Listeners To Input Fields Ends ***/

   }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*** Close Register View IBAction ***/
    
     
    @IBAction func closeRegisterView(sender: AnyObject) {
        resignFirstResponderRegisterView()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*** Register Button IBAction ***/
    @IBAction func registerButtonPressed(sender: AnyObject) {
        resignFirstResponderRegisterView()
        
        let alertTitle = "Register With Whizzbuy"
        let alertMessage = "An OTP will be sent to +91-\((mobileNumberInput.text)!) to verify your number. Do you wish to proceed?"
        
        let registerAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
            registerAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) -> Void in
            
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    /*** Reset Mobile Number Input ***/
                    self.flagMobileNumber = 0
                    self.validatorIconMobileNumber.setImage(nil, forState: UIControlState.Normal)
                    self.mobileNumberInput.text = ""
                    self.mobileNumberInput.becomeFirstResponder()
                    
                    /*** Disable Register Button ***/
                    self.registerButton.alpha = 0.5
                    self.registerButton.userInteractionEnabled = false

                    
                })

            }))
        
            registerAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    self.startActivityIndicator()
                    self.register()
                
            }))
        
        self.presentViewController(registerAlert, animated: true, completion: nil)
        
        }
    
    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/
    
    /*** Functions to Handle Top Alert Label Begins ***/
    /*** Function To Create Alert Label on View Did Load ***/
    func createAlertLabel(){
        
        alertLabel.frame = CGRectMake(0, 55, screenWidth, 0)
        alertLabel.backgroundColor = UIColor(red: 253, green: 39, blue: 94)
        alertLabel.textAlignment = NSTextAlignment.Center
        alertLabel.textColor = UIColor.whiteColor()
        alertLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 13.0)
        self.view.addSubview(alertLabel)
        
    }
    
    func slideInAlert(message: String){
        
        alertLabel.text = message
        if alertVisible == false {
                alertVisible = true
            UIView.animateWithDuration(0.25) { () -> Void in
                
                self.alertLabel.frame = CGRect(x: 0, y: 55, width: self.alertLabel.frame.width, height:40)
            }
        }
        
    }
    
    func slideOutAlert(){
        
        if alertVisible == true {
            alertVisible = false
            
            UIView.animateWithDuration(0.2) { () -> Void in
                
                self.alertLabel.frame = CGRect(x: 0, y: 55, width: self.alertLabel.frame.width, height:0)
            }
        }
        
    }
    
    /*** Functions to Handle Top Alert Label Ends ***/
    
    /*** Function for Setup of Padding and Borders Begins ***/
    func inputFieldsSetup(){
        
        /*** Hide Activity Indicator And Setup ***/
        activityIndicatorViewHolder.hidden = true
        activityIndicatorViewHolder.layer.cornerRadius = 3.0
        
        /*** Disable Register Button on Startup ***/
        registerButton.alpha = 0.5
        registerButton.userInteractionEnabled = false
        
        
        /*** Disable Confirm Password Interaction on startup ***/
        confirmPasswordInput.userInteractionEnabled = false
        
        let inputFeildsBorderColor: UIColor = UIColor(red: 229, green: 228, blue: 229)
        
        /*** Setup For First Name ***/
        scrollView.layer.addBorder(UIRectEdge.Top, color: inputFeildsBorderColor, thickness: 2.0)
        firstNameInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingViewfirstNameInput = UIView(frame: CGRectMake(0, 0, 10, self.firstNameInput.frame.height))
        firstNameInput.leftView = paddingViewfirstNameInput
        firstNameInput.leftViewMode = UITextFieldViewMode.Always

        /*** Setup For Last Name ***/
        lastNameInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingViewlastNameInput = UIView(frame: CGRectMake(0, 0, 10, self.lastNameInput.frame.height))
        lastNameInput.leftView = paddingViewlastNameInput
        lastNameInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Setup For Birthday ***/
        birthdayInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingViewbirthdayInput = UIView(frame: CGRectMake(0, 0, 10, self.birthdayInput.frame.height))
        birthdayInput.leftView = paddingViewbirthdayInput
        birthdayInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Setup For E-Mail ***/
        eMailInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingVieweMailInput = UIView(frame: CGRectMake(0, 0, 10, self.eMailInput.frame.height))
        eMailInput.leftView = paddingVieweMailInput
        eMailInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Setup For Mobile Number ***/
        mobileNumberInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingViewemobileNumberInput = UIView(frame: CGRectMake(0, 0, 10, self.mobileNumberInput.frame.height))
        mobileNumberInput.leftView = paddingViewemobileNumberInput
        mobileNumberInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Setup For Password ***/
        passwordInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingViewepasswordInput = UIView(frame: CGRectMake(0, 0, 10, self.passwordInput.frame.height))
        passwordInput.leftView = paddingViewepasswordInput
        passwordInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Set For Confirm Password ***/
        confirmPasswordInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 1.0)
        let paddingVieweconfirmPasswordInput = UIView(frame: CGRectMake(0, 0, 10, self.confirmPasswordInput.frame.height))
        confirmPasswordInput.leftView = paddingVieweconfirmPasswordInput
        confirmPasswordInput.leftViewMode = UITextFieldViewMode.Always
        
        
        /*** Set For Gender ***/
        genderInput.layer.addBorder(UIRectEdge.Bottom, color: inputFeildsBorderColor, thickness: 2.0)
        let paddingViewegenderInput = UIView(frame: CGRectMake(0, 0, 10, self.genderInput.frame.height))
        genderInput.leftView = paddingViewegenderInput
        genderInput.leftViewMode = UITextFieldViewMode.Always

    }
    /*** Function for Setup of Padding and Borders Ends ***/

    /*** Functions to Handle Keyboard Listeners Begins ***/
    func keyboardWillShow(notification:NSNotification){
        scrollViewTopConstraint.constant = 40
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        if screenHeight != 736 {
            var contentInset:UIEdgeInsets = self.scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height - assignContentInsent()
            self.scrollView.contentInset = contentInset
        }
    }
    
    func keyboardWillHide(notification:NSNotification){
        scrollViewTopConstraint.constant = 0
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollView.contentInset = contentInset
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func resignFirstResponderRegisterView(){
        UIApplication.sharedApplication().sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, forEvent: nil)
    }
    
    /*** Fucntion to Assign Content Inset for Scroll View ***/
     
    func assignContentInsent() -> CGFloat{
        
        if screenHeight == 568 {
            return 70
        }
        else {
            return 170
        }
    }
     
    /*** Functions to Handle Keyboard Listeners Ends ***/
    
    /*** Fuctions For EditingDidBegin Events of Input Fields Begins ***/
    func firstNameInputEditingDidBegin(textField: UITextField){
        
        if alertFlagFirstName == 1{
             slideInAlert("No numbers, spaces or special characters Allowed")
        }
        else{
            slideOutAlert()
        }
        
    }
    
    func lastNameInputEditingDidBegin(textField: UITextField){
        
        if alertFlagLastName == 1{
            slideInAlert("No numbers, spaces or special characters Allowed")
        }
        else{
            slideOutAlert()
        }
        
    }
    
    func birthdayInputEditingDidBegin(textField: UITextField){
        if alertFlagLastName == 1{
            slideInAlert("Minimum Age Not Met")
        }
        
        else
            if alertFlagBirthday == 2{
                slideInAlert("Invalid Date")
            }
            
        else{
            slideOutAlert()
        }
        
    }
    
    func emailInputEditingDidBegin(textField: UITextField){
        
        if alertFlagEmail == 1{
            slideInAlert("Please enter vaild E-mail")
        }
        else{
            slideOutAlert()
        }
        
    }
    
    func mobileNumberInputEditingDidBegin(textField: UITextField){
        
        if alertFlagMobileNumber == 1{
            slideInAlert("Please enter valid Mobile Number")
        }
        else{
            slideOutAlert()
        }
    }
    
    func passwordInputEditingDidBegin(textField: UITextField){
        
        if alertFlagPassword == 1{
            slideInAlert("Password needs to be a minimum of 8 characters")
        }
        else{
            slideOutAlert()
        }
        
    }
    
    func confirmPasswordInputEditingDidBegin (textField: UITextField){
        
    }
    
    /*** Fuctions For EditingDidBegin Events of Input Fields Ends ***/
     
    /*** Fucntions To Validate Input Fields Begins ***/
    func firstNameInputDidChange(){
      
    let inputValue = (firstNameInput.text)!
    var stardardText: Bool = false
    
        if inputValue.characters.count > 0{
            stardardText = testStandardTextInput(inputValue)
            
            if stardardText == true{
                flagFirstName = 1
                alertFlagFirstName = 0
                slideOutAlert()
                validatorIconFirstName.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
            }
            else
                if stardardText == false{
                    flagFirstName = 0
                    alertFlagFirstName = 1
                    slideInAlert("No numbers, spaces or special characters Allowed")
                    validatorIconFirstName.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
            }
        }
        else{
            flagFirstName = 0
            alertFlagFirstName = 0
            slideOutAlert()
            validatorIconFirstName.setImage(nil, forState: UIControlState.Normal)
        }
       enableRegisterButton()
       
    }
    
    func lastNameInputDidChange(){
        
        let inputValue = (lastNameInput.text)!
        var stardardText: Bool = false
        
        if inputValue.characters.count > 0{
            stardardText = testStandardTextInput(inputValue)
            
            if stardardText == true{
                slideOutAlert()
                alertFlagLastName = 0
                flagLastName = 1
                validatorIconLastName.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
            }
            else
                if stardardText == false{
                    flagLastName = 0
                    alertFlagLastName = 1
                    slideInAlert("No numbers, spaces or special characters Allowed")
                     validatorIconLastName.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)            }
        }
        else{
            slideOutAlert()
            flagLastName = 0
            alertFlagLastName = 0
            validatorIconLastName.setImage(nil, forState: UIControlState.Normal)
        }
        enableRegisterButton()
    }

    
    func birthdayInputValueChanged (sender: UIDatePicker){
        let apiDateFormatter = NSDateFormatter()
        let dateFormatter = NSDateFormatter()
        apiDateFormatter.dateFormat = "yyyy-MM-dd"
        
        let currentDate = NSDate()
        let enteredDate = sender.date
        
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        apiDate = apiDateFormatter.stringFromDate(sender.date)
        birthdayInput.text = dateFormatter.stringFromDate(sender.date)
        
        /*** Birthday Validation ***/
        let difference = currentDate.yearsFrom(enteredDate)
        
        if difference <= 0{
            flagBirthday = 0
            alertFlagBirthday = 2
            validatorIconBirthday.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
            slideInAlert("Invalid Date")

        }
        else
            {
                if difference >= 10{
                    flagBirthday = 1
                    alertFlagBirthday = 0
                    validatorIconBirthday.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
                    slideOutAlert()
                }
                else
                    if difference < 10{
                        flagBirthday = 0
                        alertFlagBirthday = 1
                        validatorIconBirthday.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
                        slideInAlert("Mimimum Age not met")
                }
        }
       enableRegisterButton()
    }
    
    /*** UI ToolBar Date Picker ***/
    func donePressed(sender: UIBarButtonItem) {
        
        birthdayInput.resignFirstResponder()
        
    }
    
    func eMailInputDidChange(){
        
        let inputValue: String = (eMailInput.text)!
        
        
        if inputValue.characters.count > 0{
            alertFlagEmail = 1
            slideInAlert("Please Enter valid E-Mail")
            
            if isValidEmail(inputValue){
                flagEmail = 1
                alertFlagEmail = 0
                slideOutAlert()
                validatorIconEmail.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
            }
            else {
                flagEmail = 0
                alertFlagEmail = 1
                slideInAlert("Please Enter valid E-Mail")
                validatorIconEmail.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
            }
        }
        else{
            flagEmail = 0
            alertFlagEmail = 0
            slideOutAlert()
            validatorIconEmail.setImage(nil, forState: UIControlState.Normal)
        }
        enableRegisterButton()
    }
    
    func mobileNumberInputDidChange(){
        
        let inputValue = (mobileNumberInput.text)!
        
        if inputValue.characters.count > 0{
            alertFlagMobileNumber = 1
            slideInAlert("Please enter valid Mobile Number")
            
            if inputValue.characters.count == 10{
                alertFlagMobileNumber = 0
                slideOutAlert()
                flagMobileNumber = 1
                validatorIconMobileNumber.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
            }
            else{
                alertFlagMobileNumber = 1
                slideInAlert("Please enter valid Mobile Number")
                flagMobileNumber = 0
                validatorIconMobileNumber.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
            }
        }
        else{
            flagMobileNumber = 0
            alertFlagMobileNumber = 0
            slideOutAlert()
            validatorIconMobileNumber.setImage(nil, forState: UIControlState.Normal)
        }
        enableRegisterButton()
    }
    
    func passwordInputDidChange(){
        let inputValue = (passwordInput.text)!
        
        if inputValue.characters.count > 0{
            
            if inputValue.characters.count >= 8{
                alertFlagPassword = 0
                slideOutAlert()
                confirmPasswordInput.userInteractionEnabled = true
                validationIconPassword.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
                isPasswordsMatching()
            }
            else{
                alertFlagPassword = 1
                slideInAlert("Password needs to a mimimum of 8 characters")
                validationIconPassword.setImage(UIImage(named: "validation-wrong"), forState: UIControlState.Normal)
            }
        }
        
        else{
            alertFlagPassword = 0
            slideOutAlert()
            passwordsMatchFlag = 0
            confirmPasswordInput.text = ""
            confirmPasswordInput.userInteractionEnabled = false
            validationIconPassword.setImage(nil, forState: UIControlState.Normal)
            validationIconConfirmPassword.setImage(nil, forState: UIControlState.Normal)
            
        }
        enableRegisterButton()
    }
    
    func confirmPasswordDidChange(){
        
    let passwordLength = (passwordInput.text)!.characters.count
    let confirmPasswordLength = (confirmPasswordInput.text)!.characters.count
        
        if passwordLength == 0{
                alertFlagConfirmPassword = 0
                slideOutAlert()
                validationIconConfirmPassword.setImage(nil, forState: UIControlState.Normal)
        }
        else
            if confirmPasswordLength == 0{
                 alertFlagConfirmPassword = 0
                 slideOutAlert()
                 validationIconConfirmPassword.setImage(nil, forState: UIControlState.Normal)
            }
                
        else{
                alertFlagConfirmPassword = 1
                slideInAlert("Password don't match")
                isPasswordsMatching()
        }
    }
    
    
    @IBAction func genderIndexChanged(sender: UISegmentedControl) {
        flagGender = 1
        resignFirstResponderRegisterView()
        switch sender.selectedSegmentIndex{
            
        case 0:
            genderInput.text = "Male"
            genderValue = "1"
            break
        
        case 1:
            genderInput.text = "Female"
            genderValue = "2"
            break
            
        default:
            break
        }
        enableRegisterButton()
    }
    
    
    /*** Functions To Validate Input Fields Ends ***/
    
    
    /*** Function To Test Standard Text Input ***/
    func testStandardTextInput (text: String) -> Bool{
        
        let numberSet = NSCharacterSet.decimalDigitCharacterSet()
        let specialCharacterSet = NSMutableCharacterSet()
        specialCharacterSet.addCharactersInString("\\~`!@#$%^&*()-_=+[{}]|:;'<>?,./\"")
        let whiteSpace = NSCharacterSet.whitespaceCharacterSet()
        
        let numberRange = text.rangeOfCharacterFromSet(numberSet)
        let specialCharacterRange = text.rangeOfCharacterFromSet(specialCharacterSet)
        let whiteSpaceRange = text.rangeOfCharacterFromSet(whiteSpace)
        
        
        if numberRange == nil && specialCharacterRange == nil && whiteSpaceRange == nil{
            
            return true
        }
        else {
            return false
        }
        
    }
    
    /*** Function to check valid mail using RegEx ***/
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        
        return result
    }
    
    /*** Function to check if Passwords Match ***/
    func isPasswordsMatching(){
        
        let passwordValue = (passwordInput.text)!
        let confirmPasswordValue = (confirmPasswordInput.text)!
        
        if passwordValue == confirmPasswordValue {
            alertFlagConfirmPassword = 0
            passwordsMatchFlag = 1
            slideOutAlert()
            validationIconConfirmPassword.setImage(UIImage(named: "validation-correct"), forState: UIControlState.Normal)
        }
        else{
            passwordsMatchFlag = 0
            if (confirmPasswordInput.text)!.characters.count > 0{
            validationIconConfirmPassword.setImage(UIImage(named: "validation-notEquals"), forState: UIControlState.Normal)
            }
        }
        enableRegisterButton()
    }
    
    /*** Fucntion to Enable Register Button ***/
    func enableRegisterButton(){
        
        if flagFirstName == 1 && flagLastName == 1 && flagBirthday == 1 && flagEmail == 1 && flagMobileNumber == 1 && flagMobileNumber == 1 && passwordsMatchFlag == 1 && flagGender == 1{
            
            registerButton.alpha = 1.0
            registerButton.userInteractionEnabled = true
        }
        else {
            registerButton.alpha = 0.5
            registerButton.userInteractionEnabled = false
        }
    }
    
    
    /*** API Calls and Handling ***/
    func register(){
        
        /*** Get Values From All Input Fields ***/
        let firstName = (firstNameInput.text)!
        let lastName = (lastNameInput.text)!
        let dateOfBirth = apiDate
        let eMail = (eMailInput.text)!
        let mobileNumber = (mobileNumberInput.text)!
        let password = (passwordInput.text)!
        let gender = genderValue
        
        /*** Register API URL ***/
        let registerApiURL = "https://whizzbuydev.herokuapp.com/createcustomer"
        
        let registerParameters = [
            "FirstName": firstName,
            "LastName" : lastName,
            "MobileNumber": mobileNumber,
            "EmailID": eMail,
            "DOB": dateOfBirth,
            "Password": password,
            "Gender": gender,
            "Platform": Platform
        ]
        

     self.registerAlamoFireManager?.request(.POST, registerApiURL, parameters: registerParameters, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
        
        /*** Stop Activity Indicator ***/
        self.stopActivityIndicator()
        
        if let JSON = response.result.value {
            print("Reply From Create Customer API:")
            print(JSON)
            
            if let statusCode = JSON["Status"] as! String?{
                
                self.registerApiHandling(Int(statusCode)!)
            }
            else{
                let networkErrorAlert = UIAlertController(title: "Register With Whizzbuy", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)
                
                networkErrorAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                
                self.presentViewController(networkErrorAlert, animated: true, completion: nil)
                
                print("Network Error")
            }
        }
        
     })
        
    }
    /*** Register API Call Ends ***/
    
    /*** Register API Handling Begins ***/
    
    func registerApiHandling(statusCode: Int){
    
        if statusCode == 200 {
//            print("Perform OTP SQgue")
        
            /*** Perform OTP Segue ***/
            performSegueWithIdentifier("registerOtpSegue", sender: nil)
        
        }
        else{
            generateAlertRegister(statusCode)
        }
        
    }
    
    /*** Register API Handling Ends ***/
    
    
    /*** Function to Generate Register Alerts ***/
    
    func generateAlertRegister(statusCode: Int){
        
    let alertTitle = "Register With Whizzbuy"
    var alertMessage = "Try Again"
        
        if statusCode == 1002 {
            alertMessage = "Duplicate Mobile Number"
        }
        
    let registerAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
    registerAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
    
    self.presentViewController(registerAlert, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
    if segue.identifier == "registerOtpSegue" {
        
        let otpVC: RegisterOTPViewController = segue.destinationViewController as! RegisterOTPViewController
        
        /*** Pass Mobile Number to OTP View Controller ***/
        otpVC.enteredMobileNumber = (mobileNumberInput.text)!
        }
    }


}
