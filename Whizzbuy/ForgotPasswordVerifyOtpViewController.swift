//
//  ForgotPasswordVerifyOtpViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/23/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVerifyOtpViewController: UIViewController {

/*** User Mobile Number From Forgot Password Verify Mobile Number VC***/
var enteredMobileNumber: String?

/*** AlmofireManager ***/
var ForgotPasswordVerifyOtpAlamoFireManager : Alamofire.Manager?
    
/*** All Outlets ***/
@IBOutlet weak var otpInput: UITextField!
@IBOutlet weak var messageLabel: UILabel!
@IBOutlet weak var activityIndicatorViewHolder: UIView!
@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        /*** Set OTP Input to be First Respoder ***/
        otpInput.becomeFirstResponder()
        
        /*** Alamofire Timeout Setup ***/
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 10 // seconds
        configuration.timeoutIntervalForResource = 10
        ForgotPasswordVerifyOtpAlamoFireManager = Alamofire.Manager(configuration: configuration)
        
        
        /*** Setup For OTP Text Field ***/
        otpInput.layer.cornerRadius = 5.0
        otpInput.layer.borderWidth = 2.0
        otpInput.layer.borderColor = UIColor(red: 230, green: 230, blue: 232).CGColor
        
        /*** Setup Activity Indicator ***/
        activityIndicatorViewHolder.layer.cornerRadius = 3.0
        activityIndicatorViewHolder.hidden = true
        
        
        otpInput.addTarget(self, action: #selector(ForgotPasswordVerifyOtpViewController.otpInputDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*** Fucntion OTP Input Value Did Change Begins ***/
    func otpInputDidChange(sender: UITextField){
        
        let enteredOtp = (otpInput.text)!
        
        if enteredOtp.characters.count == 4{
            self.startActivityIndicator()
            otpInput.resignFirstResponder()
            forgotPasswordVerifyOtp(enteredMobileNumber!, otp: enteredOtp)
            print(enteredOtp)
        }
        
    }
    /*** Fucntion OTP Input Value Did Change Ends ***/
    
    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/

    
    /*** Forgot Password Verify OTP API Call Begins ***/
    func forgotPasswordVerifyOtp(mobileNumber: String, otp: String){
        
        let forgotPasswordVerifyOtpURL = "https://whizzbuydev.herokuapp.com/forgotpasswordverifyOTP"
        
        let forgotPasswordVerrifyOtpParameters = [
            "MobileNumber" : mobileNumber,
            "Otp" : otp
        ]
        
        self.ForgotPasswordVerifyOtpAlamoFireManager?.request(.POST, forgotPasswordVerifyOtpURL, parameters: forgotPasswordVerrifyOtpParameters, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) in
            
            self.stopActivityIndicator()
            
            if let JSON = response.result.value {
                print("Forgot Password OTP")
                print(JSON)
                if let statusCode = JSON["Status"] as! String? {
                    
                    self.forgotPasswordVerifyOtpApiHandling(Int(statusCode)!)
                }
                
            }
                else{
                    let networkErrorAlert = UIAlertController(title: "Verify OTP", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    networkErrorAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) in
                        
                        self.forgotPasswordVerifyOtp(self.enteredMobileNumber!, otp: (self.otpInput.text)!)
                        
                    }))
                
                    networkErrorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                        
                        dispatch_async(dispatch_get_main_queue(), { 
                            
                            self.otpInput.text = ""
                            self.otpInput.becomeFirstResponder()
                            
                        })
                        
                    }))
                    
                    self.presentViewController(networkErrorAlert, animated: true, completion: nil)
                    
                    print("Network Error")
                }
            
            
        })
        
    }
     /*** Forgot Password Verify OTP API Call Ends ***/

    /*** Function for Forgot Password Verify OTP API Begins ***/
    
    func forgotPasswordVerifyOtpApiHandling(statusCode: Int){
        
        if statusCode == 200 {
            print("Good")
            //TODO: Perform Segue to Set Password
        }
        else{
            generateAlertForgotPasswordVerifyOtp(statusCode)
        }
    }
    /*** Function for Forgot Password Verify OTP API Ends ***/
    
    /*** Function to generate Alerts for Forgot Password Verify OTP Begins ***/
    func generateAlertForgotPasswordVerifyOtp(statusCode: Int){
    
        let alertTitle = "Verify OTP"
        var alertMessage = "Try Again"
        var messageLabelText = "Please Enter OTP Agian"
        
        switch statusCode {
            
        case 1000:
            otpInput.text = ""
            alertMessage = "Wrong OTP Entered. Please try Again"
            break
            
        case 1002:
            otpInput.text = ""
            messageLabelText = "You Will soon recieve an OTP via SMS from Whizzbuy. Please enter it above."
            alertMessage = "A new OTP has been genrated. Please enter the same"
            break
            
        default:
            break
        }
        
        /*** Update Message Label ***/
        messageLabel.text = messageLabelText
        
        
        let forgotPasswordVerifyOtpAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        /*** Only Make First Responder When OTP is Wrong ***/
        if statusCode != 1010 {
        
        forgotPasswordVerifyOtpAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    /*** Make OTP Input First Responder ***/
                    self.otpInput.becomeFirstResponder()
                })
                
                
            }))

        }
        else{
        forgotPasswordVerifyOtpAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            
        }
        self.presentViewController(forgotPasswordVerifyOtpAlert, animated: true, completion: nil)
    }
    
    /*** Function to generate Alerts for Forgot Password Verify OTP Ends ***/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
