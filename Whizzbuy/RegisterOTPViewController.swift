//
//  OTPViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/3/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import Alamofire

class RegisterOTPViewController: UIViewController, UITextFieldDelegate {

/*** User Mobile Number From Register VC***/
var enteredMobileNumber: String?
    
/*** AlmofireManager ***/
var RegisterOtpAlamoFireManager : Alamofire.Manager?
    
/*** OTP Correct Icon ***/
@IBOutlet weak var otpCorrectImageIcon: UIImageView!
    

/*** All Outlets ***/
@IBOutlet weak var otpInput: UITextField!
@IBOutlet weak var messageLabel: UITextView!
@IBOutlet weak var activityIndicatorViewHolder: UIView!
@IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    
override func viewDidLoad() {
    
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    /*** Make OTP Input First Responder ***/
    otpInput.becomeFirstResponder()
    
    /*** Alamofire Timeout Setup ***/
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.timeoutIntervalForRequest = 10 // seconds
    configuration.timeoutIntervalForResource = 10
    RegisterOtpAlamoFireManager = Alamofire.Manager(configuration: configuration)
    
    /*** Setup For OTP Text Field ***/
    otpInput.layer.cornerRadius = 5.0
    otpInput.layer.borderWidth = 2.0
    otpInput.layer.borderColor = UIColor(red: 230, green: 230, blue: 232).CGColor
        
    /*** Setup Activity Indicator ***/
    activityIndicatorViewHolder.layer.cornerRadius = 3.0
    activityIndicatorViewHolder.hidden = true
    
    /*** Setup Valid OTP Icon ***/
    otpCorrectImageIcon.hidden = true
    
    otpInput.addTarget(self, action: #selector(RegisterOTPViewController.otpInputDidChange), forControlEvents: UIControlEvents.EditingChanged)
    
    }
    
/*** Function For OTP Input Editing Changed ***/
func otpInputDidChange(){

let enteredOtp = (otpInput.text)!
    
    if enteredOtp.characters.count == 4{
        self.startActivityIndicator()
        otpInput.resignFirstResponder()
        verifyOtp(enteredMobileNumber!, otp: enteredOtp)
        print(enteredOtp)
    }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/

    
    /*** Verify OTP API Call Begins ***/
    
    func verifyOtp (mobileNumber: String, otp: String){
        print(otp)
        print(mobileNumber)
        let verifyOtpURL = "https://whizzbuydev.herokuapp.com/verifyotp"
        
        let verifyOtpParmeters = [
            "MobileNumber": mobileNumber,
            "Otp": otp
        ]
        print("Verify OTP Request Parameters")
        print(verifyOtpParmeters)
        
        self.RegisterOtpAlamoFireManager?.request(.POST, verifyOtpURL, parameters: verifyOtpParmeters, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            /*** Stop Activity Indicator ***/
            self.stopActivityIndicator()

            if let JSON = response.result.value{
                
                if let statusCode = JSON["Status"] as! String?{
                    print("Reply from Verify OTP API")
                    print(JSON)
                    self.verifyOtpApiHandling(Int(statusCode)!)
                }
            }
            else{
                let networkErrorAlert = UIAlertController(title: "Enter OTP", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)
                
                networkErrorAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) -> Void in
                    
                    
                    self.verifyOtp(self.enteredMobileNumber!, otp: (self.otpInput.text)!)
                    
                }))
                
                networkErrorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.otpInput.text = ""
                        self.otpInput.becomeFirstResponder()
                    })
            
                }))
                
                self.presentViewController(networkErrorAlert, animated: true, completion: nil)
                
                print("Network Error")
            }
            
        })
        
    }
    
    /*** Verify OTP API Call Ends ***/
    
    /*** Verify OTP API Handling Begins ***/
    func verifyOtpApiHandling(statusCode: Int){
        
        if statusCode == 200 {
            activityIndicatorViewHolder.hidden = false
            activityIndicator.hidden = true
            otpCorrectImageIcon.hidden = false
            
            let seconds = 1.0
            let delay = seconds * Double(NSEC_PER_SEC)
            let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            
            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("signInSegueRegister", sender:
                nil)
            })
        }
        else
        {
            generateAlertVerifyOtp(statusCode)
        }
        
    }
   /*** Verify OTP API Handling Ends ***/
    
   /*** Function to Generate Verify OTP Alerts ***/
    func generateAlertVerifyOtp(statusCode: Int){
        
        let alertTitle = "Verify OTP"
        var alertMessage = "Try Again"
        var messageLabelText = "Please Enter OTP Again"
        
        switch statusCode {
            
        case 1001:
            otpInput.text = ""
            alertMessage = "Wrong OTP Entered. Please try Again"
            break
            
        case 1002:
            otpInput.text = ""
            messageLabelText = "You Will soon recieve an OTP via SMS from Whizzbuy. Please enter it above."
            alertMessage = "A new OTP has been genrated. Please enter the same"
            break
    
        default:
            break
        }
        
        /*** Update Message Label ***/
        messageLabel.text = messageLabelText
        
        let verifyOtpAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
      
    /*** Only Make First Responder When OTP is Wrong ***/
    if statusCode != 1010 {
        verifyOtpAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
         dispatch_async(dispatch_get_main_queue(), { () -> Void in
            /*** Make OTP Input First Responder ***/
            self.otpInput.becomeFirstResponder()
         })
            
            
        }))
    }
    else{
        
        verifyOtpAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        }
        self.presentViewController(verifyOtpAlert, animated: true, completion: nil)
    }
    
}
