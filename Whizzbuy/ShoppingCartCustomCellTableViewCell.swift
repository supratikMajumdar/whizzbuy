//
//  ShoppingCartCustomCellTableViewCell.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/25/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit

class ShoppingCartCustomCellTableViewCell: UITableViewCell {

/*** UITable View Cell Components Outlets ***/
@IBOutlet weak var productNameLabel: UILabel!
@IBOutlet weak var productPriceLabel: UILabel!
@IBOutlet weak var productQuantityLabel: UILabel!
@IBOutlet weak var productTotalLabel: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
