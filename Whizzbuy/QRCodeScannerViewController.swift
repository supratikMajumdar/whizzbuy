//
//  QRCodeScanner.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/21/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import AVFoundation
import RSBarcodes_Swift

class QRCodeScannerViewController: RSCodeReaderViewController {

/*** View Outlets ***/
@IBOutlet weak var instructionsViewHolder: UIView!
@IBOutlet weak var barcodeViewer: UIView!

/*** Button Outlets ***/
@IBOutlet weak var closeButton: UIButton!
@IBOutlet weak var torchButton: UIButton!
    
    
/*** Hide Status Bar ***/ 
override func prefersStatusBarHidden() -> Bool {
        return true
}
 
/*** Boolean For Torch Status ***/
var torchStatus: Bool = false
    
override func viewDidLoad() {
        
    super.viewDidLoad()
        
    /*** Scan Only QRCodes ***/
    let types = NSMutableArray(array: self.output.availableMetadataObjectTypes)
        types.removeAllObjects()
        types.addObject(AVMetadataObjectTypeQRCode)
        self.output.metadataObjectTypes = NSArray(array: types) as [AnyObject]
        
    /*** Auto Focus Elements ***/
    self.focusMarkLayer.strokeColor = UIColor(white: 1, alpha:0.0).CGColor
    self.cornersLayer.strokeColor = UIColor.yellowColor().CGColor
        
    /*** View Initial Setup Begins ***/
    instructionsViewHolder.layer.cornerRadius = 3.0
    self.view.bringSubviewToFront(instructionsViewHolder)
    
    barcodeViewer.backgroundColor = UIColor(white: 1, alpha: 0.4)
    barcodeViewer.layer.borderWidth = 2.0
    barcodeViewer.layer.cornerRadius = 5
    barcodeViewer.layer.borderColor = UIColor(red: 254, green: 216, blue: 89).CGColor
    self.view.bringSubviewToFront(barcodeViewer)

    self.view.bringSubviewToFront(closeButton)
    self.view.bringSubviewToFront(torchButton)
    /*** View Initial Setup Ends ***/
        
    self.barcodesHandler = { barcodes in
        for barcode in barcodes {
            
            self.session.stopRunning() // Avoid scanning multiple times
            self.foundCode(barcode.stringValue)
            break

            }
        }
    }
    
/*** IBAction Fucntions ***/

    @IBAction func closeButtonPressed(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    @IBAction func torchButtonPressed(sender: AnyObject) {
        
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                if (device.torchMode == AVCaptureTorchMode.On) {
                    torchStatus = false
                    device.torchMode = AVCaptureTorchMode.Off
                } else {
                    torchStatus = true
                    try device.setTorchModeOnWithLevel(1.0)
                }
                device.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
        changeTorchImage(torchStatus)

    }
    
    /*** Function to change Torch Image ***/
    func changeTorchImage(status: Bool){
        
        if(status == false){
            torchButton.setBackgroundImage(UIImage(named: "torch-off"), forState: .Normal)
        }
        else{
            torchButton.setBackgroundImage(UIImage(named: "torch-on"), forState: .Normal)
        }
    }

    
   /*** Function For Found Code ***/
    func foundCode(qrCode: String){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            print(qrCode)
            //TODO: Segue to Shopping Cart 
          
        }
        
    }
    
    
}
