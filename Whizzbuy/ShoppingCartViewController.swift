//
//  ShoppingCartViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 3/25/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit

class ShoppingCartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

/*** All Outlets ***/
@IBOutlet weak var grandTotalLabel: UILabel!
@IBOutlet weak var totalItemsLabel: UILabel!
@IBOutlet weak var shoppingCartListTableView: UITableView!
    
/*** JSON Structure for Products in Cart ***/
var productsInCart = [AnyObject]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.barcodeScanningButton()
        /*** Limit UITableView Height to number of cells ***/
        self.shoppingCartListTableView.tableFooterView = UIView(frame: CGRectZero)
        
        }
    
    override func viewWillAppear(animated: Bool) {
        /*** Fetch Shopping Cart Data From Persistant Storage and Store ***/
        if let getProductsInCart = NSUserDefaults.standardUserDefaults().objectForKey("shoppingCart") as? [AnyObject]{
            
            productsInCart = getProductsInCart
        }
        
        /*** Set Values for Items and Grand Total ***/
        grandTotalLabel.text = "₹ " + String(getGrandTotal())
        totalItemsLabel.text = String(getTotalItems()) + " Items(s)"
        
        /*** Refresh Table View Data Each View Load ***/
        shoppingCartListTableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*** Fucntions for UITableView ***/
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productsInCart.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    /*** Fucntion Returns Dynamically Configured Cell ***/
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let shoppingCartCell = self.shoppingCartListTableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ShoppingCartCustomCellTableViewCell
        
        let productName = ((productsInCart[indexPath.row]["name"]!)!)
        let productQty = ((productsInCart[indexPath.row]["qty"]!)!)
        let productPrice = ((productsInCart[indexPath.row]["price"]!)!)

        func getProductTotal() -> Int{
            
            let price = productPrice as! Int
            let qty = productQty as! Int
            
            let productTotal = price * qty
            return productTotal
        }
        
        shoppingCartCell.productNameLabel.text = String(productName)
        shoppingCartCell.productPriceLabel.text = "₹ " + String(productPrice)
        shoppingCartCell.productQuantityLabel.text = String(productQty)
        shoppingCartCell.productTotalLabel.text = "₹ " + String(getProductTotal())
        
        
        return shoppingCartCell
    }
    
    @IBAction func closeButtonPressed(sender: UIBarButtonItem) {
        
        let closeAlert = UIAlertController(title: "Shopping Cart", message: "Are You sure Exit Shopping Cart? \n Items in the Cart will not be Saved", preferredStyle: UIAlertControllerStyle.Alert)
        
        closeAlert.addAction(UIAlertAction(title: "Exit", style: UIAlertActionStyle.Destructive, handler: { (UIAlertAction) in
        
            /*** Delete Items In Cart Before Exit ***/
            self.productsInCart.removeAll()
            NSUserDefaults.standardUserDefaults().setObject(self.productsInCart, forKey: "shoppingCart")
            
            dispatch_async(dispatch_get_main_queue(), { 
                
                self.dismissViewControllerAnimated(true, completion: nil)
                
            })
        }))
        
        closeAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(closeAlert, animated: true, completion: nil)
    }
    
    
    /*** Function To Calculate Grand Total ***/
    func getGrandTotal() -> Int{
        
        var grandTotal = 0
        
        for i in 0 ..< productsInCart.count{
            
            let unitPrice = (((productsInCart[i]["price"])!)!) as! Int
            let qty = (((productsInCart[i]["qty"])!)!) as! Int
            
            grandTotal += unitPrice * qty
        }
        
        return(grandTotal)
        
    }
    
    /*** Function To Get Total Items In Cart ***/
    func getTotalItems() -> Int{
        
        var totalItems = 0
        
        for i in 0 ..< productsInCart.count{
            
            let qty = (((productsInCart[i]["qty"])!)!) as! Int
            
            totalItems += qty
            
        }
        
        return(totalItems)
    }
    
    /*** Fucntion To Get which row in TableView is Pressed ***/
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ShoppingCartCustomCellTableViewCell
//        
//        cell.productNameLabel.text = "1"
//        
//        print(indexPath.row)
    }
    
    
    /*** Setup Function For Barcode Button Begins ***/
    func barcodeScanningButton() {
        
        var barcodeButton = UIButton(type: UIButtonType.Custom)
        barcodeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 65, height: 65))
        
        var menuButtonFrame = barcodeButton.frame
        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - 62
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        barcodeButton.frame = menuButtonFrame
        
        barcodeButton.backgroundColor = UIColor.init(red: 12, green: 198, blue: 161)
        barcodeButton.layer.cornerRadius = menuButtonFrame.height/2
        self.view.addSubview(barcodeButton)
        
        barcodeButton.setImage(UIImage(named: "barcode-scanner"), forState: UIControlState.Normal)
        
        barcodeButton.addTarget(self, action: #selector (ShoppingCartViewController.perfromSegueBarcodeScanner(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.layoutIfNeeded()
    }
    
    func perfromSegueBarcodeScanner (sender: UIButton){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.performSegueWithIdentifier("barcodeScannerSegue", sender: sender)
        
        }
        
    }
    /*** Setup Function For Barcode Button Ends ***/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
