//
//  TextField.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 2/28/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit

class TextFieldNoCopy: UITextField {

    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        
        if action == #selector(NSObject.copy(_:)) || action == #selector(NSObject.selectAll(_:)) || action == #selector(NSObject.paste(_:)) {
            
            return false
            
        }
        
        return super.canPerformAction(action, withSender: sender)
        
    }

}


class TextFieldNoCopyNoCursor: UITextField {
    
    override func caretRectForPosition(position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    override func selectionRectsForRange(range: UITextRange) -> [AnyObject] {
        return []
    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        
        if action == #selector(NSObject.copy(_:)) || action == #selector(NSObject.selectAll(_:)) || action == #selector(NSObject.paste(_:)) {
            
            return false
            
        }
        
        return super.canPerformAction(action, withSender: sender)
        
    }
    
}
