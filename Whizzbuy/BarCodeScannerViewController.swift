//
//  BarCodeScannerViewController.swift
//  Whizzbuy
//
//  Created by Supratik Majumdar on 4/5/16.
//  Copyright © 2016 supratik94. All rights reserved.
//

import UIKit
import AVFoundation
import RSBarcodes_Swift
import Alamofire

class BarCodeScannerViewController: RSCodeReaderViewController {
    
    /*** View Outlets ***/
    @IBOutlet weak var instructionsHolderView: UIView!
    @IBOutlet weak var barcodeViewer: UIView!
    
    /*** Button Outlets ***/
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var torchButton: UIButton!
    
    /*** Barcode Sound Variables ***/
    var barcodeSound = AVAudioPlayer()
    /*** Fetch File Path for BarcodeScanSound ***/
    let url = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("barcodeScanSound.mp3", ofType:nil)!)
    
    /*** Variable for Device Torch ***/
    let deviceTorch = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    var torchStatus: Bool = false
    
    /*** AlamoFireManager ***/
    var addToCartAlamoFireManager : Alamofire.Manager?
    
    /*** Variable For Fetched Product Data ***/
    var fetchedProductData: AnyObject! = nil
    
    /*** Variable For Scanned Product Barcode ***/
    var scannedProductBarCode: String! = nil
    
    /*** JSON Structure for Products in Cart ***/
    var productsInCart = [AnyObject]()
    
    /*** Activity Indicator Outlets ****/
    @IBOutlet weak var activityIndicatorViewHolder: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    /*** Hide Status Bar ***/
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*** Alamofire Timeout Setup ***/
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 10 // seconds
        configuration.timeoutIntervalForResource = 10
        addToCartAlamoFireManager = Alamofire.Manager(configuration: configuration)
        
        
        /*** BarcodeScanSound Setup ***/
        do {
            self.barcodeSound = try AVAudioPlayer(contentsOfURL: self.url)
        } catch {
            print("File not Found")
        }
        
        /*** Scan Only EAN13 Barcodes ***/
        let types = NSMutableArray(array: self.output.availableMetadataObjectTypes)
        types.removeAllObjects()
        types.addObject(AVMetadataObjectTypeEAN13Code)
        self.output.metadataObjectTypes = NSArray(array: types) as [AnyObject]
        
        /*** Auto Focus Elements ***/
        self.focusMarkLayer.strokeColor = UIColor(white: 1, alpha:0.0).CGColor
        self.cornersLayer.strokeColor = UIColor.yellowColor().CGColor

        
        /*** View Initial Setup Begins ***/
        instructionsHolderView.layer.cornerRadius = 3.0
        self.view.bringSubviewToFront(instructionsHolderView)
        
        barcodeViewer.backgroundColor = UIColor(white: 1, alpha: 0.4)
        barcodeViewer.layer.borderWidth = 2.0
        barcodeViewer.layer.cornerRadius = 5
        barcodeViewer.layer.borderColor = UIColor(red: 254, green: 216, blue: 89).CGColor
        self.view.bringSubviewToFront(barcodeViewer)
        
        self.view.bringSubviewToFront(closeButton)
        self.view.bringSubviewToFront(torchButton)
        
        activityIndicatorViewHolder.layer.cornerRadius = 3.0
        activityIndicatorViewHolder.hidden = true
        self.view.bringSubviewToFront(activityIndicatorViewHolder)
        
        /*** View Initial Setup Ends ***/

        self.barcodesHandler = { barcodes in
            for barcode in barcodes {
                
                self.session.stopRunning() // Avoid scanning multiple times
                self.foundCode(barcode.stringValue)
                break
                
            }
        }
        
    }
    
    //MARK: - viewDidAppear
    override func viewDidAppear(animated: Bool) {
        
        /*** Fetch Shopping Cart Data From Persistant Storage and Store ***/
        if let getProductsInCart = NSUserDefaults.standardUserDefaults().objectForKey("shoppingCart") as? [AnyObject]{
        
                    productsInCart = getProductsInCart
                }
                //print(productsInCart)
    }
    

    //MARK: - viewWillDisappear
    override func viewWillDisappear(animated: Bool) {
        
    /*** Store updated Shopping Cart Data in Persistance Storage ***/
    NSUserDefaults.standardUserDefaults().setObject(productsInCart, forKey: "shoppingCart")

    }
    
    /*** Functions For Handling Activity Indicator Begins ***/
    func startActivityIndicator(){
        
        activityIndicatorViewHolder.hidden = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator(){
        activityIndicatorViewHolder.hidden = true
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    /*** Functions For Handling Activity Indicator Ends ***/

    
    /*** IBActions Fucntions ***/
    @IBAction func closeButtonPressed(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func torchButtonPressed(sender: UIButton) {
        
        if (deviceTorch.hasTorch) {
            do {
                try deviceTorch.lockForConfiguration()
                if (deviceTorch.torchMode == AVCaptureTorchMode.On) {
                    deviceTorch.torchMode = AVCaptureTorchMode.Off
                    torchStatus = false
                    torchButton.setBackgroundImage(UIImage(named: "torch-off"), forState: .Normal)
                }
                else {
                    torchStatus = true
                    try deviceTorch.setTorchModeOnWithLevel(1.0)
                    torchButton.setBackgroundImage(UIImage(named: "torch-on"), forState: .Normal)
                }
                deviceTorch.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    /*** Function For Found Code ***/
    func foundCode(barCode: String){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            print(barCode)
            
            /*** Hide Barcode View Holder ***/
            self.barcodeViewer.hidden = true
            
            /*** Change Torch Image ***/
            self.torchButton.setBackgroundImage(UIImage(named: "torch-off"), forState: .Normal)
            
            /*** Start Activity Indicator***/
            self.startActivityIndicator()
            
            /*** Play Barcode Sound ***/
            self.barcodeSound.play()
            //self.dismissViewControllerAnimated(true, completion: nil)

            /*** Add to Cart API Call ***/
            self.addToCartApiCall(barCode)
        }
    }
    
    /*** Add To Cart API Call Begins ***/
    func addToCartApiCall(barCode: String){
        
    let addToCartApiURL = "https://whizzbuydev.herokuapp.com/addtocart"
     
    let addToCartParameters = [ "BarcodeNum": barCode ]
    
    self.addToCartAlamoFireManager?.request(.POST, addToCartApiURL, parameters: addToCartParameters, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) in
        
        self.stopActivityIndicator()
        
        if let JSON = response.result.value {
                self.fetchedProductData = JSON
                self.scannedProductBarCode = barCode
            if let statusCode = JSON["Status"] as! String? {
                print(statusCode)
                self.addToCartApiHandling(Int(statusCode)!)
                
            }
        }
        else{
            let networkErrorAlert = UIAlertController(title: "Add To Cart", message: "Experiencing Network Issues. Please try Again.", preferredStyle: UIAlertControllerStyle.Alert)

            networkErrorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                    self.restoreBarCodeView()
                
            }))
            
            self.presentViewController(networkErrorAlert, animated: true, completion: nil)
            
            print("Network Error")
        }

        
    })
    }
    /*** Add To Cart API Call Ends ***/
    
    /*** Add To Cart API Handling Begins ***/
    
    func addToCartApiHandling(statusCode: Int){
        
        if statusCode == 200 {
            
            /*** Check for Barcode Match ***/
            if scannedProductBarCode == fetchedProductData["barcodenumber"] as? String {
                
                    addProductToCart()
            }
            else{
                
                let addToCartScanAlert = UIAlertController(title: "Add To Cart", message: "Please Scan Item Again", preferredStyle: UIAlertControllerStyle.Alert)
                
                addToCartScanAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                    
                    self.restoreBarCodeView()
                    
                }))
                
                self.presentViewController(addToCartScanAlert, animated: true, completion: nil)
                
            }
    
            //Segue TO Shopping Cart
             segueToShoppingCart()

            
        }
        
        else {
            
            generateAlertAddToCart(statusCode)
            
        }
    }
    
    /*** Add To Cart API Handling Ends ***/
    
    /*** Fucntion To generate Add To Cart Alerts Begin ***/
    func generateAlertAddToCart(statusCode: Int){
        
        let alertTitle = "Add To Cart"
        var alertMessage = "Try Again"
        var flag = 0
        
        switch statusCode {
        case 1000:
            alertMessage = "Product Search Unsucessful"
            flag = 1
            break
            
        default:
            flag = 0
        }
        
        let addToCartAlert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        addToCartAlert.addAction(UIAlertAction(title: "Scan Again", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
            
            self.restoreBarCodeView()
            
        }))
        
        
        if flag == 1{
            
        
            addToCartAlert.addAction(UIAlertAction(title: "Go Back", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) in
                
                //Segue TO Shopping Cart
                self.segueToShoppingCart()
                
            }))
        }
        
        self.presentViewController(addToCartAlert, animated: true, completion: nil)
        
    }
     /*** Fucntion To generate Add To Cart Alerts Ends ***/
    
    func addProductToCart(){
        
        let productBarcode = fetchedProductData["barcodenumber"] as! String
        let productName = fetchedProductData["prodname"]as! String
        let productPrice = fetchedProductData["price"] as! Double
        
        var flag = 0
        
        /*** Case When No Items Are Present In the Cart ***/
        if productsInCart.count == 0{
            print("No item")
            flag = 1
            productsInCart.append(
                [
                    "barcode": productBarcode,
                    "name": productName,
                    "price": productPrice,
                    "qty": 1
                ]
            )
        }
        else{
            
            for i in 0 ..< productsInCart.count{
                
                if fetchedProductData["barcodenumber"] as! String == productsInCart[i]["barcode"] as! String {
                    print("Duplicte item")
                    flag = 1
                    
                    var quantity = productsInCart[i]["qty"] as! Int
                    quantity += 1
                    
                    productsInCart.removeAtIndex(i)
                    
                    productsInCart.insert(
                        [
                            "barcode": productBarcode,
                            "name": productName,
                            "price": productPrice,
                            "qty": quantity
                        ]
                        
                        , atIndex: i)
                }
            }
            
        }
        
        if flag == 0 {
            print("New Item")
            flag = 1
            productsInCart.append(
                [
                    "barcode": productBarcode,
                    "name": productName,
                    "price": productPrice,
                    "qty": 1
                ]
            )
        }
        
    }
    
    /*** Function To Segue To Shopping Cart ***/
    func segueToShoppingCart(){
        
       dispatch_async(dispatch_get_main_queue()) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        }
        
        
    }
    
    /*** Fucntion To Restore BarCodeView ****/
    func restoreBarCodeView (){
        
        barcodeViewer.hidden = false
        torchButton.setBackgroundImage(UIImage(named: "torch-off"), forState: .Normal)
        session.startRunning()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
